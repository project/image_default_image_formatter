INTRODUCTION
------------
If you want to set up default image separately for each view mode at manage display setting page. this module is suitable for you.

Settings you had set at manage display setting page would overwrite the same setting in field instance.

REQUIREMENTS
------------
No special requirements.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.