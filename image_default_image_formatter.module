<?php

/**
 * Implements hook_field_formatter_info().
 */
function image_default_image_formatter_field_formatter_info() {
  return array(
    'image_default_image' => array(
      'label' => t('Image default image'),
      'field types' => array('image'),
      'settings' => array(
        'image_style' => '',
        'image_link' => '',
        'image_replaced_uri' => ''
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function image_default_image_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  // Check if the formatter involves a link.
  if ($display['settings']['image_link'] == 'content') {
    $uri = entity_uri($entity_type, $entity);
  }
  elseif ($display['settings']['image_link'] == 'file') {
    $link_file = TRUE;
  }

  foreach ($items as $delta => $item) {
    if (isset($link_file)) {
      $uri = array(
        'path' => file_create_url($item['uri']),
        'options' => array(),
      );
    }
    $element[$delta] = array(
      '#theme' => 'image_formatter',
      '#item' => $item,
      '#image_style' => $display['settings']['image_style'],
      '#path' => isset($uri) ? $uri : '',
    );
  }

  return $element;
}


/**
 * Implements hook_field_formatter_settings_form().
 */
function image_default_image_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {

  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $image_styles = image_style_options(FALSE, PASS_THROUGH);
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#default_value' => isset($settings['image_style']) ? $settings['image_style'] : '',
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );

  $link_types = array(
    'content' => t('Content'),
    'file' => t('File'),
  );
  $element['image_link'] = array(
    '#title' => t('Link image to'),
    '#type' => 'select',
    '#default_value' => isset($settings['image_link']) ? $settings['image_link'] : '',
    '#empty_option' => t('Nothing'),
    '#options' => $link_types,
  );
  $element['image_replaced_uri'] = array(
    '#title' => t('Image replaced'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded image will be override the default image'),
    '#default_value' => isset($settings['image_replaced_uri']) ? $settings['image_replaced_uri'] : '',
    '#upload_location' => 'public://image_default_image_formatter/',
  );

  return $element;
}


/**
 * Implements hook_field_formatter_settings_summary().
 */
function image_default_image_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  $image_styles = image_style_options(FALSE, PASS_THROUGH);
  // Unset possible 'No defined styles' option.
  unset($image_styles['']);
  // Styles could be lost because of enabled/disabled modules that defines
  // their styles in code.
  if (isset($image_styles[$settings['image_style']])) {
    $summary[] = t('Image style: @style', array('@style' => $image_styles[$settings['image_style']]));
  }
  else {
    $summary[] = t('Original image');
  }

  $link_types = array(
    'content' => t('Linked to content'),
    'file' => t('Linked to file'),
  );
  // Display this setting only if image is linked.
  if (isset($link_types[$settings['image_link']])) {
    $summary[] = $link_types[$settings['image_link']];
  }

  if($settings['image_replaced_uri']) {
    $image = file_load($settings['image_replaced_uri']);
    if(isset($image->uri)) {
      $image_url = file_create_url($image->uri);
      //@todo need resize image.
      $summary[] = '<img width="100px" src="'.$image_url.'""/>';
    } else {
      $summary[] = 'Use default image.';
    }
  } else {
    $summary[] = 'Use default image.';
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_update_instance().
 */
function image_default_image_formatter_field_update_instance($instance, $prior_instance) {
  $act = 0;
  foreach ($instance['display'] as $display) {
    if ($display['type'] == 'image_default_image') {
      $act = 1;
    }
  }

  foreach ($prior_instance['display'] as $display) {
    if ($display['type'] == 'image_default_image') {
      $act = 1;
    }
  }

  if (!$act) {
    return;
  }

  $displays_new = $instance['display'];
  $displays_old = $prior_instance['display'];

  foreach ($displays_new as $key_new => $display_new) {
    if(isset($displays_old[$key_new])) {
      if($display_new['type'] == 'image_default_image' && $displays_old[$key_new]['type'] == 'image_default_image') {
        if(isset($display_new['settings']['image_replaced_uri']) && isset($display_old[$key_new]['settings']['image_replaced_uri']) && $display_new['settings']['image_replaced_uri'] != $displays_old[$key_new]['settings']['image_replaced_uri']) {

          $fid_old = $displays_old[$key_new]['settings']['image_replaced_uri'];
          if($fid_old) {
            $image_old = file_load($fid_old);
            file_usage_delete($image_old, 'image_default_image_formatter', $key_new, $instance['id']);
            file_delete($image_old);
          }

          $fid_new = isset($display_new['settings']['image_replaced_uri']) ? $display_new['settings']['image_replaced_uri'] : '';
          if($fid_new) {
            $image_new = file_load($fid_new);
            $image_new->status = FILE_STATUS_PERMANENT;
            file_save($image_new);
            file_usage_add($image_new, 'image_default_image_formatter', $key_new, $instance['id']);
          }
        }
      }else {
        if($display_new['type'] == 'image_default_image') {
          $fid_new = isset($display_new['settings']['image_replaced_uri']) ? $display_new['settings']['image_replaced_uri'] : '';
          if($fid_new) {
            $image_new = file_load($fid_new);
            $image_new->status = FILE_STATUS_PERMANENT;
            file_save($image_new);
            file_usage_add($image_new, 'image_default_image_formatter', $key_new, $instance['id']);
          }
        }

        if($displays_old[$key_new]['type'] == 'image_default_image') {
          $fid_old = isset($displays_old[$key_new]['settings']['image_replaced_uri']) ? $displays_old[$key_new]['settings']['image_replaced_uri'] : '';
          if($fid_old) {
            $image_old = file_load($fid_old);
            file_usage_delete($image_old, 'image_default_image_formatter', $key_new, $instance['id']);
            file_delete($image_old);
          }
        }
      }
    } else {
      if($display_new['type'] == 'image_default_image') {
        $fid_new = isset($display_new['settings']['image_replaced_uri']) ? $display_new['settings']['image_replaced_uri'] : '';
        if($fid_new) {
          $image_new = file_load($fid_new);
          $image_new->status = FILE_STATUS_PERMANENT;
          file_save($image_new);
          file_usage_add($image_new, 'image_default_image_formatter', $key_new, $instance['id']);
        }
      }
    }
  }
}

/**
 * Implements hook_field_formatter_prepare_view().
 */
function image_default_image_formatter_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  foreach($displays as $id => $display) {
    if($display['type'] == 'image_default_image') {
      if(empty($items[$id])) {
        if($displays[$id]['settings']['image_replaced_uri']) {
          $replaced_image_obj = file_load($displays[$id]['settings']['image_replaced_uri']);
          $replaced_image_array = get_object_vars($replaced_image_obj);
          $items[$id][] = $replaced_image_array;
        }
      }
    }
  }
}